
package humano;


public class Humano {

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }
    
    public double getAltura() {
        return altura;
    }
    
    public void setAltura(double altura) {
        this.altura = altura;
    }
    
    public double getIMC() {
        return IMC;
    }

    public void setIMC(double IMC) {
        this.IMC = IMC;
    }
    
     public void calcularIMC (double p, double a) {
       this.IMC=p/(a*a);
       
   } 
      public String conceptoIMC () {
          if(IMC<18.5){
               return "Su concepto de IMC es: bajo";  
       }
          else { if(IMC<24.9) {
              return "su concepto de IMC es: normal";
          }
          else {  if (IMC<29.9) {
              return "su concepto de IMC es: pero superior al normal";
              
              
          }
          else {
              return "su concepto de IMC es: obesidad";
          }
          }
              
          }
   } 
    public Humano (String nombre){
        
       this.nombre= nombre; 
       this.peso=0;
       this.altura=0;
       this.edad=0;
       this.IMC=0;
       
    }
  
      
    
    private String nombre;
    private String apellido;
    private int edad;
    private double peso;
    private double altura;
    private double IMC;

    public static void main(String[] args) {
      Humano humano1 = new Humano("sebastian");
      
      humano1.calcularIMC(40,1.72);
         System.out.println(humano1.conceptoIMC());
          humano1.calcularIMC(72,1.72);
         System.out.println(humano1.conceptoIMC());
          humano1.calcularIMC(80,1.72);
         System.out.println(humano1.conceptoIMC());
          humano1.calcularIMC(90,1.72);
         System.out.println(humano1.conceptoIMC());
    }
    
}
